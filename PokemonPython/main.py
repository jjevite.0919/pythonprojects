import random
import math


class User(object):
    def __init__(self, name):
        self.name = name
        self.pokemon = []
        self.attacking_pokemon = None

    def set_pokemon(self, set_of_pokemon):
        self.pokemon = set_of_pokemon

    def list_pokemon(self):
        for individual_pokemon in self.pokemon:
            print(individual_pokemon.name)

    def switch(self):
        picked_pokemon = False
        while not picked_pokemon:
            print("---------------------------------")
            print("Switching Pokemon!")
            for x in range(len(self.pokemon)):
                print(self.pokemon[x].name)
            name = input("Input a name from the list: ")
            for individual_pokemon in self.pokemon:
                if name == individual_pokemon.name:
                    self.attacking_pokemon = individual_pokemon
                    picked_pokemon = True
                    break

            if picked_pokemon:
                return
            else:
                print("You inputted a name that is not on the list, Try again!")

    def heal(self):
        print(self.name + " healed " + self.attacking_pokemon.name + " 20 hp!")
        self.attacking_pokemon.heal()

    def is_end_game(self):
        all_fained = True
        for individual_pokemon in self.pokemon:
            if individual_pokemon.hp > 0:
                all_fained = False
        return all_fained

    def print_attacks(self):
        print(self.attacking_pokemon.name)
        self.attacking_pokemon.print_attacks()

    def attack(self, enemy):
        found_valid_attack = False
        while not found_valid_attack:
            self.print_attacks()
            attack_name = input("Input an attack name to use: ")
            for atk_name in self.attacking_pokemon.attacks.keys():
                if attack_name == atk_name:
                    self.attacking_pokemon.attack(attack_name, enemy)
                    found_valid_attack = True

            if not found_valid_attack:
                print("Attack is not on the list")

    def check_attacking_pokemon(self):
        if self.attacking_pokemon.hp < 0:
            self.pokemon.remove(self.attacking_pokemon)
            if len(self.pokemon) == 0:
                return
            else:
                self.switch()


class Computer(User):
    def play_turn(self, enemy):
        decider = random.random()

        if decider <= 0.67:
            self.attack(enemy.attacking_pokemon)
        elif decider <= 0.83:
            self.switch()
        else:
            self.heal()

    def attack(self, enemy):
        print("Computer Attacking")
        atk_name = random.choice(list(self.attacking_pokemon.attacks.keys()))
        self.attacking_pokemon.attack(atk_name, enemy)

    def switch(self):
        print("Computer Switching")
        self.attacking_pokemon = self.pokemon[random.randint(0, len(self.pokemon)-1)]
        print(self.attacking_pokemon)

    def heal(self):
        print("Computer Healing")
        self.attacking_pokemon.heal()


class Pokemon(object):
    def __init__(self, hp, max_ap, name):
        self.hp = hp
        self.max_hp = hp
        self.max_ap = max_ap
        self.name = name
        self.knocked_out = False
        self.attacks = self.set_attacks()
        self.pokemon_type = self.set_type()

    def set_type(self):  # Handled by Inheritors
        return None

    def set_attacks(self):  # Handled by Inheritors
        self.attacks = {}

    def get_attacks(self):
        return list(self.attacks.keys())

    def print_attacks(self):
        print("Available Attacks")
        print("'Attack Name': [Base Damage, Accuracy]")
        print(self.attacks)

    def add_attacks(self, attack_dictionary):
        self.attacks = attack_dictionary

    def get_attack_power(self, attack, enemy):
        pass

    def attack(self, attack_name, enemy):
        print(self.name + " used " + attack_name + " on " + enemy.name)

        # Attack Miss Roll
        attack_chance = random.random()
        if self.attacks[attack_name][1] <= attack_chance:
            print("Attack Missed")
            return

        # Damage Calculation and Limiter
        attack_value = self.attacks[attack_name][0] * self.get_attack_power(self.attacks[attack_name], enemy)
        if attack_value > self.max_ap:
            attack_value = self.max_ap
        attack_value = math.floor(random.uniform(20, attack_value))

        print(enemy.name + " took " + str(attack_value) + " damage!")
        enemy.take_damage(attack_value)

    def take_damage(self, damage_amount):
        self.hp -= damage_amount

    def heal(self):
        self.hp += 20
        if self.hp > self.max_hp:
            self.hp = self.max_hp

    # Added
    def print_stats(self):
        print(self.name)
        print(self.hp)


class GrassType(Pokemon):
    def __init__(self, hp, max_ap, name):
        super().__init__(hp, max_ap, name)

    def set_type(self):
        return 'grass'

    def set_attacks(self):
        # grass_attacks = {'Leaf Storm': [130, 0.9], 'Mega Drain': [50, 1.0], 'Razor Leaf': [55, 0.95]}
        # return grass_attacks
        self.attacks = {'Leaf Storm': [130, 0.9], 'Mega Drain': [50, 1.0], 'Razor Leaf': [55, 0.95]}
        return self.attacks

    def get_attack_power(self, attack, enemy):
        if enemy.pokemon_type == 'water':
            print("It is super effective!")
            return 1.5
        else:
            return 1


class WaterType(Pokemon):
    def __init__(self, hp, max_ap, name):
        super().__init__(hp, max_ap, name)

    def set_type(self):
        return 'water'

    def set_attacks(self):
        self.attacks = {'Bubble': [40, 1.0], 'Hydro Pump': [185, 0.3], 'Surf': [70, 0.9]}
        return self.attacks

    def get_attack_power(self, attack, enemy):
        if enemy.pokemon_type == 'fire':
            print("It is super effective!")
            return 1.5
        else:
            return 1


class FireType(Pokemon):
    def __init__(self, hp, max_ap, name):
        super().__init__(hp, max_ap, name)

    def set_type(self):
        return 'fire'

    def set_attacks(self):
        self.attacks = {'Ember': [60, 1.0], 'Fire Punch': [85, 0.8], 'Flame Wheel': [70, 0.9]}
        return self.attacks

    def get_attack_power(self, attack, enemy):
        if enemy.pokemon_type == 'grass':
            print("It is super effective!")
            return 1.5
        else:
            return 1


def create_user():
    name = input("What is your name?: ")
    new_user = User(name)
    return new_user


def set_user_pokemon(user, pokemon_list):
    new_pokemon_list = []
    picked_pokemon = 0
    while picked_pokemon < 3:
        print("--------------------------------")
        print("Pick 3 Pokemons from the following")
        for individual_pokemon in pokemon_list:
            print(individual_pokemon.name)
        name = input("Input a name from the list: ")
        found_match = False
        for individual_pokemon in pokemon_list:
            if name == individual_pokemon.name:
                new_pokemon_list.append(pokemon_list.pop(pokemon_list.index(individual_pokemon)))
                picked_pokemon += 1
                found_match = True
                break

        if found_match:
            continue
        else:
            print("You inputted a name that is not on the list, Try again!")

    user.set_pokemon(new_pokemon_list)
    user.attacking_pokemon = user.pokemon[0]


def set_computer_pokemon(computer, pokemon_list):
    new_pokemon_list = []
    for index in range(3):
        new_pokemon_list.append(pokemon_list.pop(random.randint(0, len(pokemon_list)-1)))

    computer.set_pokemon(new_pokemon_list)
    computer.attacking_pokemon = computer.pokemon[0]


def print_stats(user):
    print(user.name)
    for poke in user.pokemon:
        poke.print_stats()


def player_input(player, computer):
    valid_input = False
    while not valid_input:
        choice = input("Do you want to Attack, Switch, or Heal?: ")
        if choice == 'Attack':
            player.attack(computer.attacking_pokemon)
            valid_input = True
        elif choice == 'Switch':
            player.switch()
            valid_input = True
        elif choice == 'Heal':
            player.heal()
            valid_input = True
        else:
            print("Invalid Choice, Try again")


def game_loop():
    player = create_user()
    computer = Computer("Computer")

    pokemon_list = [
        GrassType(60, 40, 'Bulbasoar'),
        GrassType(40, 60, 'Bellsprout'),
        GrassType(50, 50, 'Oddish'),
        FireType(25, 70, 'Charmainder'),
        FireType(30, 50, 'Ninetails'),
        FireType(40, 60, 'Ponyta'),
        WaterType(80, 20, 'Squirtle'),
        WaterType(70, 40, 'Psyduck'),
        WaterType(50, 50, 'Polywag')]

    game_over = False

    set_user_pokemon(player, pokemon_list)
    set_computer_pokemon(computer, pokemon_list)

    player.list_pokemon()
    computer.list_pokemon()

    # player.switch(0)
    # player.heal()
    # player.print_attacks()
    # player.attack(computer.attacking_pokemon)
    # print(computer.attacking_pokemon.hp)
    # computer.attack(player.attacking_pokemon)
    # print(player.attacking_pokemon.hp)
    #
    # player.attack(computer.attacking_pokemon)
    # print(computer.attacking_pokemon.hp)
    # player.check_attacking_pokemon()
    # computer.check_attacking_pokemon()
    #
    # player.list_pokemon()
    # computer.list_pokemon()

    # pokemon_list[0].print_hp()
    #pokemon_list[6].print_hp()
    #pokemon_list[0].attack('Razor Leaf', pokemon_list[6])
    #pokemon_list[6].print_hp()
    while not game_over:
        print_stats(player)
        print_stats(computer)

        player_input(player, computer)
        computer.play_turn(player)

        if player.is_end_game():
            print("All " + player.name + " pokemon has fainted, Game Over!")
            game_over = True
            break
        elif computer.is_end_game():
            print("All " + computer.name + " pokemon has fainted, " + player.name + " Wins!")
            game_over = True
            break

        player.check_attacking_pokemon()
        computer.check_attacking_pokemon()


game_loop()
